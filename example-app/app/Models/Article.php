<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $appends = [
        'full_image_src',
        'preview_image_src'
    ];

    protected $fillable = [
        'date',
        'name',
        'preview_image',
        'full_image',
        'shortDesc',
        'desc',
    ];
    public function getFullImageSrcAttribute() {
        return '/images/'.$this->full_image;
    }

    public function getPreviewImageSrcAttribute() {
        return '/images/'.$this->preview_image;
    }
}
