<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index() {
        $articles = Article::all();
        return view('articles', ['articles'=>$articles]);
    }
    public function get($articleId) {
        $article = Article::findOrFail($articleId);
        return view('article', ['article'=>$article]);
    }
}
