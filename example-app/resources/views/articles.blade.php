
@include('template.header')

<main class="main">
    <h1 class="articles__heading">
        Новости
    </h1>

    <ul class="articles__list">
        @foreach($articles as $article)
            <li class="articles__item">
                <a href="/articles/{{$article->id}}" class="articles__name">{{$article->name}}</a>

                <p class="articles__short-desc">{{$article->shortDesc}}</p>

                <a href="/articles/{{$article->id}}" class="articles__name">
                    <img src="{{$article->preview_image_src}}" class="articles__preview-img" alt="preview image" >
                </a>
            </li>

        @endforeach
    </ul>

</main>

@include('template.footer')
