<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lab 1,2</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/reseter.css">
</head>
<body >
    <div class="wrapper">
        <header class="header">
            <nav class="header__navigation navigation">
                <ul class="navigation__list">
                    <li class="navigation__item">
                        <img src="/images/logo.png" alt="logo" class="header__logo">
                    </li>
                    <li class="navigation__item">
                        <a href="/"  class="navigation__link">
                            Главная
                        </a>
                    </li>
                    <li class="navigation__item">
                        <a href="/articles"  class="navigation__link">
                            Новости
                        </a>
                    </li>
                    <li class="navigation__item">
                        <a href="/aboutus"  class="navigation__link">
                            О нас
                        </a>
                    </li>
                </ul>
            </nav>
        </header>
