
@include('template.header')

<main class="main">
    <h1 class="article__name">{{$article->name}}</h1>
    <p class="article__date">{{$article->date}}</p>
    <p class="article__desc">{{$article->desc}}</p>
    @if($article->full_image)
        <img src="{{$article->full_image_src}}" alt="full image" class="article__img">
    @endif
</main>

@include('template.footer')



